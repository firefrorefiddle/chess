(ns chess.intface)

(def standard-board 
  [[:w  :b  :w  :b  :w  :b  :w  :b]
   [:b  :w  :b  :w  :b  :w  :b  :w]
   [:w  :b  :w  :b  :w  :b  :w  :b]
   [:b  :w  :b  :w  :b  :w  :b  :w]
   [:w  :b  :w  :b  :w  :b  :w  :b]
   [:b  :w  :b  :w  :b  :w  :b  :w]
   [:w  :b  :w  :b  :w  :b  :w  :b]
   [:b  :w  :b  :w  :b  :w  :b  :w]])

(defn simple? [cell]
  (or (= :b cell) (= :w) cell))

(defn cell-color [cell] 
  (cond (= :w cell) :w
        (= :b cell) :b
        (map? cell) (:color cell)))

(defn cell-set-piece [board coords piece]
  (let [old-cell (get-in board coords)
        new-cell (if (simple? old-cell)
                   { :color (cell-color old-cell) :content { :type :piece :value piece }}
                   (assoc old-cell :content {:type :piece :value piece}))]
    (assoc-in board coords new-cell)))

(defn clear-cell [board coords]
  (assoc-in board coords (get-in board (conj coords :color))))

(defn set-position [position]
  (into []
        (map (fn [prow brow] (map (fn [piece color]
                                    (if (nil? piece) color
                                        { :color color :content { :type :piece :value piece }})) 
                                  prow brow))
             (:board position) standard-board)))

(defn add-coordinates [board [cols lines]]
  (let [to-cell (fn [c t] {:content {:value c :type t}})
        ccols   (into [] (map #(to-cell % :x-coordinate) cols))
        llines  (into [] (map #(to-cell % :y-coordinate) lines))]
    (map #(into [%1] %2) 
         (conj llines "")
         (conj board ccols))))

(def standard-coordinates [["a" "b" "c" "d" "e" "f" "g" "h"] [8 7 6 5 4 3 2 1]])
(def flipped-coordinates (map reverse standard-coordinates))

(defn to-board [sseq]
  (into [] (map #(into [] %) sseq)))

(defn flip-board [b]
  (to-board (reverse (map reverse b))))

(defn piece-filename [piece]
  (str "pieces/"
       ({:p "bp" :k "bk" :q "bq" :r "br" :n "bn" :b "bb"
         :P "wp" :K "wk" :Q "wq" :R "wr" :N "wn" :B "wb"} piece)
       ".svg"))


(defn board->html [board & [flipped]]
  (let [cwhite    "cell white"
        cblack    "cell black"
        cxcoord   "cell x-coordinate"
        cycoord   "cell y-coordinate"
        cpiece    "piece"

        draw-coord (fn [class value cell]
                     (assoc-in (conj cell value)
                               [1 :class] 
                               class))

        draw-piece (fn [piece] [:div {:class cpiece}
                                [:object {:height "100%"
                                          :width "100%"
                                          :data (piece-filename piece)
                                          :type "image/svg+xml"}]])

        draw-content (fn [content cell]
                       (cond
                         (= :x-coordinate (:type content)) (draw-coord cxcoord (:value content) cell)
                         (= :y-coordinate (:type content)) (draw-coord cycoord (:value content) cell)
                         (= :piece (:type content)) (conj cell (draw-piece (:value content)))
                         :else cell))                       

        draw-cell (fn [cell lineidx colidx]
                    (draw-content (:content cell) [:td {:id (when (cell-color cell) (str colidx lineidx))
                                                        :class (case (cell-color cell)
                                                                 :w cwhite
                                                                 :b cblack
                                                                 "")}]))

        draw-line (fn [line lineidx]  [:tr (map (fn [cell colidx] (draw-cell cell lineidx colidx)) 
                                                line 
                                                (seq "xabcdefgh"))])

        draw      (fn [board] [:table {:class "board"} (map draw-line board (range 8 -1 -1))])]

    (if flipped
      (draw (add-coordinates (flip-board board) flipped-coordinates))
      (draw (add-coordinates board standard-coordinates)))))

