(ns chess.compat)

(defn parse-int [str]
  (#?(:clj Integer/parseInt
      :cljs js/parseInt)
   str))

(defn log [& args]
  (#?(:clj println
      :cljs js/console.log)
   (apply str args)))

(defn to-char-code [str]
  #?(:clj (-> str (get 0) int)
     :cljs (.charCodeAt str 0)))
