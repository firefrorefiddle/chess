(ns chess.rules
  (:use [chess.board 
         :only [color-at coords-valid? offset-coords index-board color opp-color piece-at
                board-move algebraic->ints]]
        [clojure.set :only [intersection difference]])
  (:require [chess.compat :as compat]))

(def starting-position
  {:board [[ :r  :n  :b  :q  :k  :b  :n  :r]
           [ :p  :p  :p  :p  :p  :p  :p  :p]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [ :P  :P  :P  :P  :P  :P  :P  :P]
           [ :R  :N  :B  :Q  :K  :B  :N  :R]]
   :ep nil
   :castle #{:w00 :w000 :b00 :b000}
   :to-move :w})

(def empty-position
  {:board [[nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]
           [nil nil nil nil nil nil nil nil]]
   :ep nil
   :castle #{:w00 :w000 :b00 :b000}
   :to-move :w})

(def starting-board 
  (:board starting-position))


(defn friendly? [board coords my-color]
  (= (color-at board coords) my-color))

(defn opponent? [board coords my-color]
  (and (color-at board coords) 
       (not (friendly? board coords my-color))))

(def capture? opponent?)

(defn simple-moves [directions board coords my-color]
  (->> directions
       (map #(offset-coords coords %))
       (keep coords-valid?)
       (keep #(if (friendly? board % my-color) 
                nil
                %))))

(defn straight-moves-by [board start direction breaks-inc breaks-exc]
  (let [beam  (iterate #(offset-coords % direction) start)
        beams (map vector beam (rest beam))
        break (take-while (fn [[this next]]
                            (and (not-any? #(and (not (= start this))
                                                 (% this)) breaks-inc)
                                 (not-any? #(% next)
                                           (conj breaks-exc
                                                 #(not (coords-valid? %)))))) 
                          beams)]
    (map second break)))

(defn straight-moves [board start direction my-color]
  (straight-moves-by board start direction 
                     [#(capture? board % my-color)] 
                     [#(friendly? board % my-color)]))

(defn knight-moves-b [board coords my-color]
  (simple-moves [:nnw :nne :wnw :ene :wsw :ese :ssw :sse] board coords my-color))

(defn knight-moves [position coords my-color]
  (knight-moves-b (:board position) coords my-color))

(defn king-moves-b [board coords my-color]
  (simple-moves [:nw :n :ne :e :se :s :sw :w] board coords my-color))

(defn king-moves [position coords my-color]
  (king-moves-b (:board position) coords my-color))
       
(defn rook-moves-b [board coords my-color]
  (apply concat (map #(straight-moves board coords % my-color) [:n :e :s :w])))

(defn rook-moves [position coords my-color]
  (rook-moves-b (:board position) coords my-color))

(defn bishop-moves-b [board coords my-color]
  (apply concat (map #(straight-moves board coords % my-color) [:nw :ne :se :sw])))

(defn bishop-moves [position coords my-color]
  (bishop-moves-b (:board position) coords my-color))

(defn queen-moves-b [board coords my-color]
  (concat (rook-moves-b board coords my-color) (bishop-moves-b board coords my-color)))

(defn queen-moves [position coords my-color]
  (queen-moves-b (:board position) coords my-color))

(defn pawn-moves [position coords my-color]
  (let [board         (:board position)
        forward-dir   (if (= my-color :w) :n :s)
        capture-dirs  (if (= my-color :w) [:nw :ne] [:sw :se])
        starting-row  (if (= my-color :w) 6 1)
        num-forward   (if (= (get coords 0) starting-row) 2 1)
        forward-moves (take num-forward 
                            (straight-moves-by board coords forward-dir nil 
                                               [#(friendly? board % my-color)
                                                #(capture? board % my-color)]))
        capture-moves (filter #(capture? board % my-color)
                              (simple-moves capture-dirs board coords my-color))
        ep-move       (cond (= (:ep position) (offset-coords coords :w))
                            (simple-moves [(capture-dirs 0)] board coords my-color)
                            (= (:ep position) (offset-coords coords :e))
                            (simple-moves [(capture-dirs 1)] board coords my-color))]
    (into [] (concat forward-moves capture-moves ep-move))))
                             
(def piece-moves
  {:p pawn-moves   :P pawn-moves
   :r rook-moves   :R rook-moves
   :n knight-moves :N knight-moves
   :b bishop-moves :B bishop-moves
   :q queen-moves  :Q queen-moves
   :k king-moves   :K king-moves})

(defn raw-moves [position my-color]
  (let [iboard (index-board (:board position))
        pieces (filter #(= (-> % (get 1) (color)) my-color) iboard)
        moves  (map (fn [[coords piece]]
                      (map #(vector coords %)
                           ((get piece-moves piece) position coords my-color)))
                    pieces)]
    (apply concat moves)))


(defn attacks [position my-color]
  (map second (raw-moves position my-color)))

(defn castle-moves [position my-color]
  (let [castles {:w {:w00  (sorted-set [7 4] [7 5] [7 6])
                     :w000 (sorted-set [7 4] [7 3] [7 2])}
                 :b {:b00  (sorted-set [0 4] [0 5] [0 6])
                     :b000 (sorted-set [0 4] [0 3] [0 2])}}
        att    (into #{} (attacks position (opp-color my-color)))
        legal? (fn [[key ixs]]
                 (and (contains? (:castle position) key)
                      (empty? (intersection ixs att))
                      (not-any? #(color-at (:board position) %) (rest ixs))))
        legals (filter legal? (get castles my-color))]
    (map (fn [[_ squares]] [(first squares) (last squares)]) legals)))

(defn pawn-double-move? [position from to]
  (let [piece (piece-at (:board position) from)]
    (when (and (#{:p :P} piece)
             (#{2 -2} (- (get from 0) (get to 0))))
      to)))

(defn ep-move? [position from to]
  (let [piece (piece-at (:board position) from)]
    (when (and (#{:p :P} piece)
               (nil? (piece-at (:board position) to))
               (#{1 -1} (- (get from 1) (get to 1))))
      to)))

(defn castle-move? [position from to]
  (let [piece (piece-at (:board position) from)]
    (cond (= :k piece) #{:b00 :b000}
          (= :K piece) #{:w00 :w000}
          (and (= :r piece) (= from [0 0])) #{:b000}
          (and (= :r piece) (= from [0 7])) #{:b00}
          (and (= :R piece) (= from [7 0])) #{:w000}
          (and (= :R piece) (= from [7 7])) #{:w00}
          :else #{})))
    
(defn move [position from to]
  (let [board   (:board position)]
    (-> position
        (assoc :board  (let [b (board-move board from to)]
                         (if (ep-move? position from to)
                           (assoc-in b (:ep position) nil)
                           b)))
        (assoc :ep     (pawn-double-move? position from to))
        (assoc :castle (difference (:castle position)
                                   (castle-move? position from to)))
        (assoc :to-move (opp-color (color-at board from))))))

(defn check? [position def-color]
  (let [opp-color (opp-color def-color)
        king      (case def-color :w :K :b :k)]
    (some (fn [square] 
            (= king (piece-at (:board position) square)))
          (attacks position opp-color))))
                 
(defn legal-moves [position]

  (let [my-color (:to-move position)
        raws    (raw-moves position my-color)
        castles (castle-moves position my-color)]

    (apply vector (filter (fn [fromto]
                            (let [from (get fromto 0)
                                  to (get fromto 1)] 
                              (not (check? (move position from to) my-color))))
                          (into raws castles)))))

(defn legal-moves-from [position from]
  (let [test  (fn [fromto] (= from (get fromto 0)))]
    (apply vector (filter test (legal-moves position)))))

(defn mate? [position]
  (and (check? position (:to-move position))
       (empty? (legal-moves position))))

(defn stalemate? [position]
  (and (not (check? position (:to-move position)))
       (empty? (legal-moves position))))

