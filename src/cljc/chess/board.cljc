(ns chess.board
  (:require [chess.compat :as compat]))

(defn index-board [board]
  (let [index-row (fn [row i]
                    (map #(vector [i %2] %1) row (range 0 8)))]                      
    (apply concat (map index-row board (range 0 8)))))

(defn ints->algebraic [[y x]]
  [(-> x (+ 97) char str keyword) (- 8 y)])

(defn move->algebraic [[from to]]
  [(ints->algebraic from) (ints->algebraic to)])

(defn algebraic->ints [[x y]]
  [(- 8 y) (-> x name compat/to-char-code (- 97))])

(defn any->ints [xy]
  (cond 
    (string? xy) 
    (algebraic->ints [(keyword (subs xy 0 1))
                      (compat/parse-int (subs xy 1 2))])
    (keyword? (first xy)) (algebraic->ints xy)
    :else xy))

(defn ints->string [yx]
  (let [a (ints->algebraic yx)]
    (str (name (get a 0)) (get a 1))))


(defn coords-valid? [[y x]]
  (when (and (< -1 y 8) (< -1 x 8))
    [y x]))

(def offsets
  {; simple moves
   :nw [-1 -1] :n [-1  0] :ne [-1  1]
    :w [ 0 -1]             :e [ 0  1]
   :sw [ 1 -1] :s [ 1  0] :se [ 1  1]
   ; knight moves
          :nnw [-2 -1] :nne [-2 1]
     :wnw [-1 -2]              :ene [-1 2]
     :wsw [ 1 -2]              :ese [ 1 2]
          :ssw [2  -1] :sse [ 2 1]})

(defn offset-coords [[y x] dir]
  (let [[yd xd] (offsets dir)]
    [(+ yd y) (+ xd x)]))

(defn black? [piece]
  (and piece
       (<= (compat/to-char-code "a") 
           (compat/to-char-code (name piece))
           (compat/to-char-code "r"))))

(defn white? [piece]
  (and piece
       (<= (compat/to-char-code "A") 
           (compat/to-char-code (name piece))
           (compat/to-char-code "R"))))

(defn opp-color [color]
  (case color
    :w :b
    :b :w))

(defn color [piece] 
  (cond 
    (black? piece) :b
    (white? piece) :w))

(defn piece-at [board coords]
  (get-in board coords))

(defn color-at [board coords]
  (->> coords (get-in board) color))

(defn set-piece [board coords piece]
  (assoc-in board coords piece))

(defn clear-square [board coords]
  (assoc-in board coords nil))

(defn board-move [board from to]
  (-> board
      (set-piece to (piece-at board from))
      (clear-square from)))
