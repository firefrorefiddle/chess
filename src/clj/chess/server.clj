(ns chess.server
  (:use ring.adapter.jetty
        ring.middleware.resource
        ring.util.response
        [ring.middleware.content-type :only [wrap-content-type]]
        hiccup.core
        [hiccup.page :only [include-css include-js]]
        compojure.core
        chess.rules
        chess.intface)
  (:require [compojure.route :as route]))


(defn board [request]
  (html [:html
         [:head
          (include-css "chess.css")]
         [:body {:onload "chess.client.init_client();"}
          (include-js "main.js")
          (board->html (set-position empty-position))]]))

(defroutes board-routes
  (GET "/board.html" [] board))

(def app 
  (-> board-routes
      (wrap-resource "public")
      wrap-content-type))

