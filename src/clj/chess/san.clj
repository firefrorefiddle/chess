(ns chess.san
  (:use chess.rules
        chess.board
        clojure.core))

(def normal-move-re #"([NBRQK]?)([a-h]?)([1-8]?)(x?)([a-h])([1-8])(=[NBRQK])?([+#]?)")
(def castle-re #"(O-O(-O)?)")
(def move-re (re-pattern (str normal-move-re "|" castle-re "|" #"(\.\.\.)")))

(def prelude-re #"(?m)^\[(\w+)\s+\"([^\"]+)\"\]")
(def comment-re #"(?m)\{([^{]*)\}|;(.*)$")
(def moves-re #"(\d+)\.\s*(\w+)(?:\s+(\w+))?")

(defn maybe [val f] 
  (when (and val (not (= val "")))
    (f val)))

(defn r-int [string]
  (maybe string #(. Integer parseInt %)))

(defn read-normal-move [movestr]
  (when-let [match (re-matches move-re movestr)]
    (let [[move p ff fr x tf tr pr m] match]
      {:move move 
       :piece (if (= p "") "P" p) 
       :from-file (maybe ff keyword) 
       :from-rank (r-int fr)
       :to-file (keyword tf)
       :to-rank (r-int tr)
       :capture? (= x "x") 
       :check? (= m "+")
       :mate? (= m "#")
       :promote (maybe pr #(keyword (subs % 1 2)))})))

(defn read-castle [movestr]
  (when-let [match (re-matches castle-re movestr)]
    {:move (first match)
     :castle? true
     :capture? false
     :check? false
     :mate? false
     :queenside? (when (second match) true)}))

(defn read-move [movestr]
  (or (read-normal-move movestr) 
      (read-castle movestr)
      (when (re-matches #"\.\.\." movestr) nil)))

(defn read-moves [gamestr]
  (let [spool-fwd (fn [queue]
                    (let [[start m]  (first queue)
                          idx    (.end m)
                          queue' (if (.find m)
                                   (assoc (dissoc queue start) (.start m) m)
                                   (dissoc queue start))
                          [skip keep] (split-with 
                                        (fn [[midx m]]
                                          (<= midx idx)) queue')]
                      (into (into (sorted-map) keep) (map (fn [[midx m]]
                                                            (when (.find m)
                                                              (println (str "new entry (index " (.start m) "): " m))
                                                              [(.start m) m])) skip))))
        m-move     (re-matcher move-re gamestr)
        m-movenum  (re-matcher #"(\d+)\." gamestr)
        m-comment  (re-matcher comment-re gamestr)
        init-queue (into (sorted-map)
                         (map (fn [m]
                                (when (.find m)
                                  (vector (.start m) m))) [m-move m-movenum m-comment]))]
    (loop [queue   init-queue
           move    {}
           movenum 0
           color   :w
           acc     []]
      (if (empty? queue) 
        acc
        (let [[start m] (first queue)
              groups (re-groups m)]
          (cond 
            
            (= m m-move)    (recur (spool-fwd queue)
                                   {}
                                   movenum
                                   (opp-color color)
                                   (conj acc
                                         (merge (assoc move :color color :num movenum)
                                                (read-move (first groups)))))
            (= m m-movenum) (recur (spool-fwd queue)
                                   move
                                   (. Integer parseInt (second groups))
                                   color
                                   acc)
            (= m m-comment) (recur (spool-fwd queue)
                                   (assoc move :comment (first groups))
                                   movenum
                                   color
                                   acc)))))))
                         
(defn re-seq-e
  [^java.util.regex.Pattern p str]
  (let [m (re-matcher p str)]
    ((fn step []
       (when (. m (find))
         (cons {:match (re-groups m)
                :start (.start m)
                :end (.end m)} (lazy-seq (step))))))))

(defn read-pgn [pgnstr]
  (let [matches   (re-seq-e prelude-re pgnstr)
        tags      (for [[_ tag value] (map :match matches)]
                    {:tag tag :value value})
        gamestr   (subs pgnstr
                        (-> matches last :end)
                        (.length pgnstr))]
    [tags (read-moves gamestr)]))

(defn find-move-from-san [position move]
  "Does not check x, + or # for now."
  (let [color (:to-move position)
        legal (legal-moves position)
        candidates (filter (fn [[from to]]
                             (let [from-a (ints->algebraic from)
                                   to-a   (ints->algebraic to)
                                   piece  (piece-at (:board position) from)]
                               (and (= to-a [(:to-file move) (:to-rank move)])
                                    (or (nil? (:from-file move)) 
                                        (= (:from-file move) (get from-a 0)))
                                    (or (nil? (:from-rank move)) 
                                        (= (:from-rank move) (get from-a 1)))
                                    (= (:piece move)
                                       (.toUpperCase (name piece))))))
                           legal)]
    (if (not (= (count candidates) 1))
      :illegal-or-ambiguous
      (first candidates))))

(defn san->gametree 
  ([moves] 
     (san->gametree moves starting-position))
  ([moves initial-position]
     (loop [tree [initial-position]
            position initial-position
            san-moves moves]
       (println (first san-moves))
       (if (empty? san-moves) tree
           (let [m (find-move-from-san position (first san-moves))]
                   (println position)
                   (println (first m))
                   (println (second m))
                   (println m)
             (if (keyword? m) m
                 (let [position' (move position (first m) (second m))]
                   (recur
                    (conj tree [m position'])
                    position'
                    (rest san-moves)))))))))
