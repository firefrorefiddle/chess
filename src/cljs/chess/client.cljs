(ns chess.client
  (:require 
    [chess.rules :as rules]
    [chess.board :as board]
    [chess.compat :as compat]
    [chess.show :as show]))

(def state (atom {}))

(declare click! move-selected!)

(defn ^:export init-client []
  (show/set-position! rules/starting-position)
  (swap! state assoc :position rules/starting-position)
  (swap! state assoc :legal-moves (rules/legal-moves rules/starting-position))
  (swap! state assoc :selected nil)
  (swap! state assoc :attacked #{})
  (show/add-click-handlers! click!))

(defn selected? [id]
  (= id (:selected @state)))

(defn selected []
  (:selected @state))

(defn attacked? [id]
  (some #{id} (:attacked @state)))

(defn movable? [id]
  (some #{id} (map (comp board/ints->string first) (:legal-moves @state))))

(defn unattack! [id]
  (show/unattack! id)
  (swap! state (fn [s] (assoc s :attacked (disj (:attacked s) id)))))

(defn attack! [id]
  (swap! state (fn [s] (assoc s :attacked (conj (:attacked s) id))))
  (show/attack! id))

(defn unselect! [id]

  (when (selected? id)
    (show/unselect! id)
    (swap! state assoc :selected nil)

    (doseq [att (:attacked @state)]
      (unattack! att))

    (js/console.log (str "unselected " id))))

(defn select! [id]

  (when (:selected @state)
    (unselect! (:selected @state)))

  (swap! state assoc :selected id)
  (show/select! id)

  (let [legal-moves (filter #(= (get % 0) (board/any->ints id)) (:legal-moves @state))]
    (doseq [att legal-moves]
      (attack! (-> att second board/ints->string))))

  (js/console.log (str "selected " id)))

(defn click! [id]
  (compat/log "user clicked on " id)
  (cond (selected? id) (unselect! id)
        (attacked? id) (move-selected! id)
        (movable? id) (select! id)
        (selected) (unselect! (selected))
        :else nil))        

(defn legal [from to]
  (some #{[(board/any->ints from) (board/any->ints to)]} (:legal-moves @state)))

(defn move! [from to]

  (if (not (legal from to))
    (compat/log "illegal move attempted: " from " - " to)

    (let [from' (board/any->ints from)
          to' (board/any->ints to)]

      (when (selected)
        (unselect! (selected)))

      (swap! state assoc :position (rules/move (:position @state) from' to'))
      (swap! state assoc :legal-moves (rules/legal-moves (:position @state)))

      (show/set-position! (:position @state)))))

         
(defn move-selected! [id]
  (move! (selected) id))
