(ns chess.show
  (:require 
   [dommy.utils :as utils]
   [dommy.core :as dommy]
   [chess.board :as board]
   [chess.compat :as compat]
   [chess.intface :as if]))

(defn mkid [id]
  (keyword (str "#" id)))

(defn ^:export add-click-handlers! [handlerfn]  
  (doseq [file "abcdefgh"
          rank [1 2 3 4 5 6 7 8]]   
    (let [id (str file rank)
          handler (fn [_] (handlerfn id))]
      (dommy/listen! (dommy/sel1 (mkid id)) :click handler))))

(defn clear-square! [id]
  (dommy/clear! (dommy/sel1 [(mkid id)])))

(defn clear-board! []
  (doseq [file "abcdefgh"
          rank [1 2 3 4 5 6 7 8]]   
    (clear-square! (str file rank))))

(defn set-piece! [square piece]
  (let [n (node [:img {:class "piece"
                       :src (if/piece-filename piece)}])]
    (dommy/append! (dommy/sel1 (mkid square)) n)))

(defn ^:export set-position! [position]
  (clear-board!)
  (doseq [x [0 1 2 3 4 5 6 7]
          y [0 1 2 3 4 5 6 7]]
    (when-let [p (get-in position [:board y x])]
      (set-piece! (board/ints->string [y x]) p))))

(defn ^:export attack! [id]
  (-> (dommy/sel1 (mkid id))
      (dommy/add-class! :attacked)))

(defn ^:export unattack! [id]
  (-> (dommy/sel1 (mkid id))
      (dommy/remove-class! :attacked)))

(defn ^:export select! [id]
  (-> (dommy/sel1 (mkid id))
      (dommy/add-class! :selected)))

(defn ^:export unselect! [id]
  (-> (dommy/sel1 (mkid id))
      (dommy/remove-class! :selected)))



