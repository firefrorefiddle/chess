(ns chess.dnd
  (:require 
   [dommy.core :as dommy]
   [chess.compat :as compat]))

(defonce state (atom {}))

(defn logevent [str e]
  (compat/log str " " 
              (.-screenX e) "/" 
              (.-screenY e) "/" 
              (.-button e) "/" 
              (.-buttons e) "/" 
              (.-relatedTarget e)))

(defn handle-drag [from to]
  (compat/log "dragged from " from " to " to))

(defn mousemove [e]
  (logevent "mouse moved" e))

(defn mouseup [id e] 
  (swap! state (fn [s]
                 (when (= :drag (:state s))
                   (handle-drag (:start-cell s) id))
                 {})))

(defn mousedown [id e] 
  (compat/log "mouse down id " id)
  (swap! state (fn [s] (-> s
                           (assoc :state :drag)
                           (assoc :start-cell id)
                           (assoc :start-x (.-screenX e))
                           (assoc :start-y (.-screenY e))))))

(defn init []
  (doseq [element (dommy/sel [:table.board :td.cell])]
    (dommy/listen! element :mouseup (fn [e] (mouseup (.-id element) e)))
    (dommy/listen! element :mousedown (fn [e] 
                                        (.preventDefault e)
                                        (mousedown (.-id element) e)))))

