(defproject chess "0.1.0-SNAPSHOT"
  :description "An online chess manager program"
  :url "http://firefrorefiddle.github.com/chess"
  :license {:name "proprietary license; all rights reserved"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [ring "1.5.0"]
                 [ring/ring-json "0.4.0"]
                 [compojure "1.5.1"]
                 [hiccup "1.0.5"]
                 [enlive "1.1.6"]
                 [org.clojure/clojurescript "1.9.229"]
                 [prismatic/dommy "1.1.0"]]
  :plugins [[lein-swank "1.4.5"]
            [lein-cljsbuild "1.1.4"]
            [lein-ring "0.9.7"]]
  :dev-dependencies [[lein-ring "0.9.7"]]
  :ring {:handler chess.server/app
         :nrepl { :start? true }}
  :source-paths ["src/clj"
                 "src/cljc"]
  :resource-paths ["resources"]
  :cljsbuild {:builds [{:source-paths ["src/cljs"
                                       "src/cljc"]
                        :compiler {:output-to "resources/public/main.js"
                                   :optimizations :whitespace
                                   :pretty-print true}}]})
