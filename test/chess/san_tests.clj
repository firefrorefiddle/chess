(ns 'test.chess.san
  (:use clojure.test))

(deftest succeed []
  (is (+ 1 1) 2))

(deftest fail []
  (is (+ 1 1) 3))
